# gims (Global Information Management System)
**gims** is a project dealing with the topic of information management.
The idea is to build a scalable information management system that can be integrated into many services.
This is not only about the storage of login data but also about the storage of general information about a person and their data.

## Help
Everybody is welcome to join the project and add missing information and ideas.
If you have ideas or if you are missing something that cannot be added as information, please create an issue.
The current list of information that can be saved can be found [here](gims-overview.md).

## Status
And the current project status can be found [here](status.md).